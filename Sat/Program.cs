﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using MySql.Data;
using MySql.Data.MySqlClient;



namespace Sat
{
    public class Prensa
    {
        public string Codigo { get; set; }
        public string Numero { get; set; }
        public string Satelite { get; set; }
        public Prensa(string codigo, string numero, string satelite)
        {
            Codigo = codigo;
            Numero = numero;
            Satelite = satelite;
        }



        public string asignarValor(string codigoNumero)
        {
            //Recepcion de codigo o numero de prensa y lo guardo en la variable codigoNumero
            bool flagVacio = true;
            while (flagVacio)
            {
                Console.Write("Ingresar codigo o numero de prensa. Sino cerrar el programa desde la ventana.\n");
                codigoNumero = Console.ReadLine();
                if (!string.IsNullOrEmpty(codigoNumero))
                {
                    flagVacio = false;
                }
                else
                {
                    Console.WriteLine("Ingreso un valor vacio, ingrese algun valor\n");
                    Console.WriteLine("Presione cualquier tecla para continuar\n");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
            return codigoNumero;
        }


        public void traerPrensa(string codigoNumero)
        {
            try
            {
                //Busco si hay alguna letra dentro de el codigoPrensa que me pasaron
                bool result = false;
                int indexLetter = 0;
                int limite = codigoNumero.Length;
                while (indexLetter < limite && result == false)
                {
                    result = Char.IsLetter(codigoNumero, indexLetter); //isletter busca si hay un caracter y devuelve un true si lo hay, de esta forma se si buscar por cod prensa o nro prensa
                    indexLetter++;
                }

                if (result)
                {
                    this.Satelite = this.auxConexionDb(codigoNumero, "cod_prensa");
                }
                else
                {
                    this.Satelite = this.auxConexionDb(codigoNumero, "nro_prensa");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine( );
                Console.Write(string.Concat("Error: ", ex.Message.ToString() ));
            }

            
        }


        public string auxConexionDb(string codigoNumero, string columna)
        {
           // string sat = "0";
            string connStr = "server=localhost;user=root;database=satelite;port=3306;password=";
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                //Console.WriteLine("Conectando a la base de datos...");
                conn.Open();
                string sql = "SELECT satelite FROM prensa WHERE " + columna + " = @codigoNumero";
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@codigoNumero", codigoNumero);
                MySqlDataReader reader = cmd.ExecuteReader();
                DataTable dtconsulta = new DataTable();
                dtconsulta.Load(reader);
                DataRow row = dtconsulta.Rows[0];
                return row["satelite"].ToString();
                 

            }
            catch (Exception /*ex*/)
            {
                //throw ex;
                return "0";
                //Console.WriteLine(ex.ToString());
            }
            finally
            {

                conn.Close();
            }
            //Console.WriteLine("Funciona ok.");
                    }
        
    }




    class Program
    {
        
        
        static void Main()
        {
            while (true)
            {
                //Instaciar las clases y crear variables, metodos
                Prensa prensa1 = new Prensa("0", "0", "0");
                string codigoNumero ="";

                //tomo valor de prensa del usuario por teclado
                codigoNumero = prensa1.asignarValor(codigoNumero);
                
                //traigo la prensa y la agrego al objeto que instancie
                prensa1.traerPrensa(codigoNumero);

                //Imprimo a que prensa pertence, si devuelve 0 es porque no trajo el satelite correcto
                if (prensa1.Satelite != "0")
                {
                    Console.WriteLine("Esta prensa pertenece al satelite numero {0}", prensa1.Satelite);
                }
                else
                {
                    Console.WriteLine("Escribio mal la prensa, o esta prensa no existe");
                }
            
                //Vuelvo al inicio del programa

                // Dejamos la consola en debbuggeo.
                Console.WriteLine("Apreta cualquier tecla.");
                Console.ReadKey();
                Console.Clear();

            }
        }
    }
}
/*        ayudas recordatorios
        Console.WriteLine("Nombre de la prensa 1 = {0} Numero = {1}", prensa1.Codigo, prensa1.Numero);
        // Declaramos un nuevo satelite y le damos el valor de satelite1.
        Prensa prensa2 = prensa1;
        //Cambiamos los datos de prensa 2, y tambien cambiamos los de prensa 1.
        prensa2.Codigo = "c07";
        prensa2.Age = 16;
        Console.WriteLine("Nombre de la prensa 1 = {0} edad = {1}", prensa2.Codigo, prensa2.Numero);
        Console.WriteLine("Nombre de la prensa 2 = {0} edad = {1}", prensa1.Codigo, prensa1.Numero);
        
                DataRow r2 = new DataRow();
                foreach (DataRow r1 in dtconsulta.Rows)
                {
                    if (r1["cod_prensa"].ToString() = codigoNumero) {
                        r2 = r1;
                        return;
                    }
                }*/

                /*if (result != null)
                {
                    //imprimo para debug
                    Console.WriteLine(reader);
                }
                aprender mas sobre
                foreach buscar
                buscar datatable 
                siempre que hago un metodo meter un try catch
*/
